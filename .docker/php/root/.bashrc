#!/bin/bash

export PROMPT_COMMAND='history -a; history -c; history -r'

alias ll='ls -la'

function phpunit() {
    /root/src/vendor/bin/phpunit "$@"
}

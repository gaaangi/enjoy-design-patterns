# Decorator

## Decorator パターンとは？

中心となるオブジェクトを、飾り付けとなる機能を付加するオブジェクトでラッピングし組み合わせることで、目的に沿うオブジェクトを作り上げるパターン

## パターンの構成要素

- Component
  - 機能を追加する核となるオブジェクト
  - 基本的なインターフェースだけを持つ
- ConcreteComponent
  - Component のインターフェースを実装する
- Decorator
  - Component と同じインターフェースを持つ
  - 機能を付加する対象の Component を持つ
- ConcreteDecorator
  - Decorator のインターフェースを実装する

## 関連パターン

- Composite
  - 共通点
    - Composite も Decorator もオブジェクト間に再帰的な構造をつくる
  - 相違点
    - Composite パターンは「容器と中身の同一視」
      - 容器（Composite）と中身（Leaf）を同一の部品（Component）として扱えるようにし、再帰的な構造をつくることを目的とする
    - Decorator パターンは「装飾と本体の同一視」
      - 装飾（Decorator）と本体（Component）を同一視することで、本体に機能を付加することを目的とする
- Adapter
  - Decorator パターンは本体（Component）のインターフェースを変えることなく機能を付加する
  - Adapter パターンはずれのある 2 つのインターフェースをつなぐ
- Strategy
  - Decorator パターンは装飾（Decorator）を組み合わせたり交換することによって機能を変更する
  - Strategy パターンはアルゴリズムを切り替えることによって機能を変更する

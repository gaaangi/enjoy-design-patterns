<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * コーヒーカップ > 容量
 * @method static CupSize SHORT()
 * @method static CupSize TALL()
 * @method static CupSize GRANDE()
 * @method static CupSize VENTI()
 */
class CupSize extends Enum
{
    private const SHORT  = 240;
    private const TALL   = 350;
    private const GRANDE = 470;
    private const VENTI  = 590;

    public function gt(CupSize $size): bool
    {
        return $this->getValue() > $size->getValue();
    }
}

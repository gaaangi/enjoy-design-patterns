<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * コーヒーカップ > 色
 * @method static CupColor BLUE()
 * @method static CupColor GREEN()
 * @method static CupColor RED()
 * @method static CupColor YELLOW()
 */
class CupColor extends Enum
{
    private const BLUE   = 'blue';
    private const GREEN  = 'green';
    private const RED    = 'red';
    private const YELLOW = 'yellow';
}

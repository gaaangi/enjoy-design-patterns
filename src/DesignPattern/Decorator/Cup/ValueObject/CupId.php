<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\ValueObject;

/**
 * コーヒーカップ > ID
 */
class CupId
{
    private int $id;
    
    public function __construct(int $id)
    {
        $this->id = $id;
    }
    
    public function getValue(): int
    {
        return $this->id;
    }
}

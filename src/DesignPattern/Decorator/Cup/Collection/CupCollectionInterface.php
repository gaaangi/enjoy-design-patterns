<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\Collection;

use DesignPattern\Decorator\Cup\Cup;
use DesignPattern\Decorator\Cup\ValueObject\CupId;

interface CupCollectionInterface
{
    public function size(): int;

    public function add(Cup $cup): void;

    public function pop(): Cup;

    public function sort(CupCollectionComparatorInterface $sorter): void;

    /**
     * @return Cup[]
     */
    public function toArray(): array;

    /**
     * @return CupId[]
     */
    public function toCupIdArray(): array;
}

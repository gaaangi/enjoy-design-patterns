<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\Collection;

use DesignPattern\Decorator\Cup\Cup;

class CupLimitCollection extends AbstractCupCollectionDecorator
{
    private int $limit;

    public function __construct(CupCollectionInterface $cupCollection, int $limit)
    {
        parent::__construct($cupCollection);
        $this->limit = $limit;

        // adjust size on initialization
        if ($this->cupCollection->size() > $limit) {
            for ($i = 0; $i < $this->cupCollection->size() - $limit; ++$i) {
                $this->cupCollection->pop();
            }
        }
    }

    public function add(Cup $cup): void
    {
        $this->cupCollection->add($cup);
        if ($this->cupCollection->size() > $this->limit) {
            $this->cupCollection->pop();
        }
    }
}

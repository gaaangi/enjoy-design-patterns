<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\Collection;

use DesignPattern\Decorator\Cup\Cup;

class CupSortedCollection extends AbstractCupCollectionDecorator
{
    private CupCollectionComparatorInterface $sorter;

    public function __construct(CupCollectionInterface $cupCollection, CupCollectionComparatorInterface $sorter)
    {
        parent::__construct($cupCollection);
        $this->sorter = $sorter;

        // sort on initialization
        $this->sort($this->sorter);
    }

    public function add(Cup $cup): void
    {
        $this->cupCollection->add($cup);
        $this->cupCollection->sort($this->sorter);
    }
}

<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\Collection;

use DesignPattern\Decorator\Cup\Cup;

class CupCollection implements CupCollectionInterface, \IteratorAggregate
{
    /**
     * @var Cup[]
     */
    private array $cups = [];

    public function size(): int
    {
        return count($this->cups);
    }

    public function add(Cup $cup): void
    {
        $this->cups[$cup->getId()->getValue()] = $cup;
    }

    public function pop(): Cup
    {
        $last = array_key_last($this->cups);
        $deletedCup = $this->cups[$last];
        unset($this->cups[$last]);

        return $deletedCup;
    }

    public function sort(CupCollectionComparatorInterface $sorter): void
    {
        uasort(
            $this->cups,
            static function (Cup $a, Cup $b) use ($sorter): int {
                return $sorter->compare($a, $b);
            }
        );
    }

    public function toArray(): array
    {
        return array_values($this->cups);
    }

    public function toCupIdArray(): array
    {
        $cupIdArray = [];
        foreach ($this->cups as $cup) {
            $cupIdArray[] = $cup->getId();
        }

        return $cupIdArray;
    }

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->cups);
    }
}

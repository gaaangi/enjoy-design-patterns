<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\Collection;

use DesignPattern\Decorator\Cup\Cup;

abstract class AbstractCupCollectionDecorator implements CupCollectionInterface
{
    protected CupCollectionInterface $cupCollection;

    public function __construct(CupCollectionInterface $cupCollection)
    {
        $this->cupCollection = $cupCollection;
    }

    public function add(Cup $cup): void
    {
        $this->cupCollection->add($cup);
    }

    public function pop(): Cup
    {
        return $this->cupCollection->pop();
    }

    public function size(): int
    {
        return $this->cupCollection->size();
    }

    public function sort(CupCollectionComparatorInterface $sorter): void
    {
        $this->cupCollection->sort($sorter);
    }

    public function toArray(): array
    {
        return $this->cupCollection->toArray();
    }

    public function toCupIdArray(): array
    {
        return $this->cupCollection->toCupIdArray();
    }

    public function getIterator(): \ArrayIterator
    {
        return $this->cupCollection->getIterator();
    }
}

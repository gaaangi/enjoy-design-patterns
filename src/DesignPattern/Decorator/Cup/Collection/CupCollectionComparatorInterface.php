<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\Collection;

use DesignPattern\Decorator\Cup\Cup;

interface CupCollectionComparatorInterface
{
    /**
     * $a よりも $b が後に来るべきときに true を返す
     */
    public function compare(Cup $a, Cup $b): int;
}

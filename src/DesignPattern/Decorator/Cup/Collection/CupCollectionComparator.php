<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup\Collection;

use DesignPattern\Decorator\Cup\Cup;

class CupCollectionComparator implements CupCollectionComparatorInterface
{
    public function compare(Cup $a, Cup $b): int
    {
        return $a->isLargerThan($b) ? -1 : 1;
    }
}

<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Cup;

use DesignPattern\Decorator\Cup\ValueObject\CupColor;
use DesignPattern\Decorator\Cup\ValueObject\CupId;
use DesignPattern\Decorator\Cup\ValueObject\CupSize;

/**
 * コーヒーカップ
 */
class Cup
{
    private CupId $id;

    private CupColor $color;

    private CupSize $size;

    public function __construct(CupId $id, CupColor $color, CupSize $size)
    {
        $this->id = $id;
        $this->color = $color;
        $this->size = $size;
    }

    public function getId(): CupId
    {
        return $this->id;
    }

    public function getColor(): CupColor
    {
        return $this->color;
    }

    public function getSize(): CupSize
    {
        return $this->size;
    }

    public function isLargerThan(Cup $cup): bool
    {
        return $this->size->gt($cup->getSize());
    }
}

<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Display;

abstract class Border extends Display
{
    protected Display $display;

    public function __construct(Display $display)
    {
        $this->display = $display;
    }
}

<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Display;

class SideBorder extends Border
{
    private string $borderChar;

    /**
     * @throws \Exception
     */
    public function __construct(Display $display, string $borderChar)
    {
        if (mb_strlen($borderChar) !== 1) {
            throw new \Exception();
        }

        parent::__construct($display);
        $this->borderChar = $borderChar;
    }

    public function getColumns(): int
    {
        return 1 + $this->display->getColumns() + 1;
    }

    public function getRows(): int
    {
        return $this->display->getRows();
    }

    public function getRowText(int $row): string
    {
        return $this->borderChar . $this->display->getRowText($row) . $this->borderChar;
    }
}

<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Display;

class StringDisplay extends Display
{
    private string $string;

    public function __construct(string $string)
    {
        $this->string = $string;
    }

    public function getColumns(): int
    {
        return strlen($this->string);
    }

    public function getRows(): int
    {
        return 1;
    }

    public function getRowText(int $row): string
    {
        if ($row === 0) {
            return $this->string;
        }

        return '';
    }
}

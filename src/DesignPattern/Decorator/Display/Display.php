<?php
declare(strict_types=1);

namespace DesignPattern\Decorator\Display;

abstract class Display
{
    public abstract function getColumns(): int;

    public abstract function getRows(): int;

    public abstract function getRowText(int $row): string;

    public final function show(): void
    {
        for ($i = 0; $i < $this->getRows(); ++$i) {
            fwrite(STDOUT, $this->getRowText($i) . "\n");
        }
    }
}

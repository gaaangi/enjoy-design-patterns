<?php
declare(strict_types=1);

namespace Tests\DesignPattern\Decorator\Display;

use DesignPattern\Decorator\FullBorder;
use DesignPattern\Decorator\SideBorder;
use DesignPattern\Decorator\StringDisplay;
use PHPStan\Testing\TestCase;

class DecoratorPatternTest extends TestCase
{
    public function testDecoratorPattern(): void
    {
        $d1 = new StringDisplay("Hello, world.");
        $d2 = new SideBorder($d1, '#');
        $d3 = new FullBorder($d2);
        $d1->show();
        $d2->show();
        $d3->show();

        $d4 = new SideBorder(new FullBorder(new FullBorder(new SideBorder(new FullBorder(new StringDisplay('Hello')), '*'))), '/');
        $d4->show();
    }
}

<?php
declare(strict_types=1);

namespace Tests\DesignPattern\Decorator\Cup;

use DesignPattern\Decorator\Cup\Collection\CupCollection;
use DesignPattern\Decorator\Cup\Collection\CupCollectionComparator;
use DesignPattern\Decorator\Cup\Collection\CupLimitCollection;
use DesignPattern\Decorator\Cup\Collection\CupSortedCollection;
use DesignPattern\Decorator\Cup\Cup;
use DesignPattern\Decorator\Cup\ValueObject\CupColor;
use DesignPattern\Decorator\Cup\ValueObject\CupId;
use DesignPattern\Decorator\Cup\ValueObject\CupSize;
use PHPStan\Testing\TestCase;

class CupCollectionTest extends TestCase
{
    public function testCupCollection(): void
    {
        $cup1 = new Cup(new CupId(1), CupColor::BLUE(), CupSize::SHORT());
        $cup2 = new Cup(new CupId(2), CupColor::GREEN(), CupSize::GRANDE());
        $cup3 = new Cup(new CupId(3), CupColor::BLUE(), CupSize::TALL());
        $cup4 = new Cup(new CupId(4), CupColor::RED(), CupSize::VENTI());
        $cup5 = new Cup(new CupId(5), CupColor::YELLOW(), CupSize::GRANDE());

        $cupCollection = new CupCollection();
        $cupCollection->add($cup1);
        $cupCollection->add($cup2);
        $cupCollection->add($cup3);
        $cupCollection->add($cup4);
        $cupCollection->add($cup5);

        self::assertEquals([$cup1, $cup2, $cup3, $cup4, $cup5], $cupCollection->toArray());
    }

    public function testCupSortedCollection(): void
    {
        $cup1 = new Cup(new CupId(1), CupColor::BLUE(), CupSize::SHORT());
        $cup2 = new Cup(new CupId(2), CupColor::GREEN(), CupSize::GRANDE());
        $cup3 = new Cup(new CupId(3), CupColor::BLUE(), CupSize::TALL());
        $cup4 = new Cup(new CupId(4), CupColor::RED(), CupSize::VENTI());

        $cupSortedCollection = new CupSortedCollection(new CupCollection(), new CupCollectionComparator());
        $cupSortedCollection->add($cup1);
        $cupSortedCollection->add($cup2);
        $cupSortedCollection->add($cup3);
        $cupSortedCollection->add($cup4);

        self::assertEquals([$cup4, $cup2, $cup3, $cup1], $cupSortedCollection->toArray());
    }

    public function testCupSortedLimitCollection(): void
    {
        $cup1 = new Cup(new CupId(1), CupColor::BLUE(), CupSize::SHORT());
        $cup2 = new Cup(new CupId(2), CupColor::GREEN(), CupSize::GRANDE());
        $cup3 = new Cup(new CupId(3), CupColor::BLUE(), CupSize::TALL());
        $cup4 = new Cup(new CupId(4), CupColor::RED(), CupSize::VENTI());

        $cupSortedLimitCollection = new CupLimitCollection(new CupSortedCollection(new CupCollection(), new CupCollectionComparator()), 3);
        $cupSortedLimitCollection->add($cup2);
        $cupSortedLimitCollection->add($cup3);
        $cupSortedLimitCollection->add($cup1);
        $cupSortedLimitCollection->add($cup4);

        self::assertEquals([$cup4, $cup2, $cup3], $cupSortedLimitCollection->toArray());
    }
}
